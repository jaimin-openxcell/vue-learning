export const fruitMixin = {
    data() {
        return {
            fruits: ['mango', 'banan', 'Peach', 'Kiwi'],
            filterText: ''
        }
    },
    computed: {
        filteredFruits() {
            return this.fruits.filter((item) => {
                return item.match(this.filterText);
            });
        }
    }
};
